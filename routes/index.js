var express = require('express');
var router = express.Router();
const ObjectID = require('mongodb').ObjectID;

//  FOR TESTING PURPOSES  
router.get('/', (req, res, next) => {
  res.json("200 OK");
});

//  Route for retrieving a user's flash card session
router.get('/flash-card/:id', (req, res, next) => {
  const { id } = req.params;
  const _id = ObjectID(id);

  req.collection.findOne({ _id })
    .then(results => res.json(results))
    .catch(error => res.send(error));
});

//  Route for getting all question
router.get('/questions', (req, res, next) => {
  req.app.locals.db.collection('questions').find({})
    .toArray()
    .then(results => res.json(results))
    .catch(error => res.send(error));
});

//  Route for teachers posting a question
router.post('/questions', (req, res, next) => {
  const {id, question, correct, wrong, difficulty} = req.body;
  const payload = {id, question, correct, wrong, difficulty};
  req.app.locals.db.collection('questions')
    .insertOne(payload)
    .then(result => res.json(result.ops[0]))
    .catch(error => res.send(error));
});

//  Route for students saving their flash card session
router.post('/flash-card', (req, res, next) => {
  const { unansweredDeck, wrongDeck, correctDeck } = req.body;
  const payload = { unansweredDeck, wrongDeck, correctDeck };
  req.collection.insertOne(payload)
    .then(result => res.json(result.ops[0]))
    .catch(error => res.send(error));
});

router.delete('/flash-card', (req, res, next) => {
  //const { id } = req.params;
  //const _id = ObjectID(id);

  req.collection.remove({})
    .then(result => res.json(result))
    .catch(error => res.send(error));
})

module.exports = router;